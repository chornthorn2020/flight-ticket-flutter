import 'package:flightticket/CustomShapeClipper.dart';
import 'package:flightticket/main.dart';
import 'package:flutter/material.dart';

final Color discountBackgroundColor = Color(0xFFFFE08D);
final Color flightBorderColor = Color(0xFFE6E6E6);
final Color chipBackgroundColor = Color(0xFFF6F6F6);


class FlightListingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Search Result'),
        centerTitle: true,
        leading: InkWell(
        child: Icon(Icons.arrow_back),
          onTap:() => Navigator.pop(context),
        ),
      ),
      body: Column(
        children: <Widget>[
          FlightTopPart(),
        ],
      ),
    );
  }
}

class FlightTopPart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: CustomShapeClipper(),
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [firstColor,secondColor])
            ),
            height: 160.0,
          ),
        ),
        Column(
          children: <Widget>[
            SizedBox(height: 20.0),
            Card(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
              margin: EdgeInsets.symmetric(horizontal: 16.0),
              elevation: 10.0,
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Phnom Penh',style: TextStyle(fontSize: 16.0),),
                          Divider(color: Colors.grey,height: 20.0,),
                          Text('Bangkok (BK)',style: TextStyle(fontSize: 16.0,fontWeight: FontWeight.bold),)
                        ],
                      ),
                      Icon(Icons.import_export,color: Colors.black,)
                    ],
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}












