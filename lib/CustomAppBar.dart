import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {

  final List<BottomNavigationBarItem> bottomBarItems = [];

  final bottomNavigationBarStyle =
      TextStyle(color: Colors.black, fontStyle: FontStyle.normal);

  CustomAppBar() {
    bottomBarItems.add(BottomNavigationBarItem(
      icon: Icon(
        Icons.home,
        color: Colors.black,
      ),
      title: Text('Explore', style: bottomNavigationBarStyle),
    ));
    bottomBarItems.add(BottomNavigationBarItem(
      icon: Icon(
        Icons.favorite,
        color: Colors.black,
      ),
      title: Text('Watchlist', style: bottomNavigationBarStyle),
    ));
    bottomBarItems.add(BottomNavigationBarItem(
      icon: Icon(
        Icons.local_offer,
        color: Colors.black,
      ),
      title: Text('Deals', style: bottomNavigationBarStyle),
    ));
    bottomBarItems.add(BottomNavigationBarItem(
      icon: Icon(
        Icons.notifications,
        color: Colors.black,
      ),
      title: Text('Notifications', style: bottomNavigationBarStyle),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 15.0,
      child: BottomNavigationBar(
        items: bottomBarItems,
        type: BottomNavigationBarType.fixed,
      ),
    );
  }
}
